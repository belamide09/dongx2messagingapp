package com.example.dong2x.dongx2messagingapp;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.dong2x.dongx2messagingapp.MyObjects.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class ComposeMessage extends AppCompatActivity {
    WebView txt_to;
    EditText txt_message;
    ImageButton btn_send, btn_add;
    ArrayList<Contact> contact_list;
    ArrayList<Contact> receipient_selection;
    ArrayList<Contact> receipient_list;
    ArrayList<HashMap<String, String>> SentMessageList;
    Dialog dialog_contacts;
    ListView sent_message_list_view;
    ArrayList<HashMap<String, String>> receipientList;
    SimpleAdapter receipient_selection_adapter;
    SimpleAdapter adapter;

    private WindowManager windowManager;
    private ImageView chatHead;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_message);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_compose_message);
            actionBar.setTitle("Compose Message");
        }

        // initialize element(s)
        txt_to = (WebView) findViewById(R.id.txt_to);
        txt_message = (EditText) findViewById(R.id.txt_message);
        btn_send = (ImageButton) findViewById(R.id.btn_send_message);
        btn_add = (ImageButton) findViewById(R.id.btn_add);

        contact_list = new ArrayList<>();
        receipient_selection = new ArrayList<>();
        receipient_list = new ArrayList<>();

        sent_message_list_view = (ListView)findViewById(R.id.sent_message_list);

        Contact contact1 = new Contact();
        contact1.id = "1";
        contact1.name = "John";
        contact1.number = "+639325045595";
        contact1.organization = "Phil INC.";
        receipient_selection.add(contact1);
        contact_list.add(contact1);

        Contact contact2 = new Contact();
        contact2.id = "2";
        contact2.name = "James";
        contact2.number = "+639223556360";
        contact2.organization = "TESTED";
        receipient_selection.add(contact2);
        contact_list.add(contact2);

        Contact contact3 = new Contact();
        contact3.id = "3";
        contact3.name = "Migz";
        contact3.number = "+639226661737";
        contact3.organization = "NAZE";
        receipient_selection.add(contact3);
        contact_list.add(contact3);

        Contact contact4 = new Contact();
        contact4.id = "4";
        contact4.name = "Glenson";
        contact4.number = "+639336103533";
        contact4.organization = "ZAZE";
        receipient_selection.add(contact4);
        contact_list.add(contact4);

        Contact contact5 = new Contact();
        contact5.id = "5";
        contact5.name = "Sang Don";
        contact5.number = "+639321403733";
        contact5.organization = "ATE";
        receipient_selection.add(contact5);
        contact_list.add(contact5);

        Contact contact6 = new Contact();
        contact6.id = "6";
        contact6.name = "Richard";
        contact6.number = "+639436755077";
        contact6.organization = "THEY";
        receipient_selection.add(contact6);
        contact_list.add(contact6);

        Contact contact7 = new Contact();
        contact7.id = "7";
        contact7.name = "Torres";
        contact7.number = "+639363788757";
        contact7.organization = "BALAY";
        receipient_selection.add(contact7);
        contact_list.add(contact7);

        Util.animateOnclickView(btn_add, 2, 200, btnAddClickEventListener);
        Util.animateOnclickView(btn_send, 1, 200, btnSendMessage);

        initializeListview();

        displayReceipient();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    protected Util.UtilCallback btnAddClickEventListener
            = new Util.UtilCallback() {

        @Override
        public void method(View view) {
            /* if dialog_contacts is already initialize */
            if (dialog_contacts != null) {
                dialog_contacts.show();
                adapter.notifyDataSetChanged();
                return;
            }

            // Initialize a new instance of LayoutInflater service
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

            // Inflate the custom layout/view
            View customView = inflater.inflate(R.layout.receipient_list_container, null);
            final ListView lv = (ListView) customView.findViewById(R.id.receipient_list_view);
            ImageButton btn_close = (ImageButton)customView.findViewById(R.id.btn_close);

            receipientList = new ArrayList<HashMap<String, String>>();

            for(int x=0;x<receipient_selection.size();x++){
                HashMap hash = new HashMap();
                hash.put("id",  String.valueOf(receipient_selection.get(x).id));
                hash.put("name",  receipient_selection.get(x).name);
                hash.put("number",  receipient_selection.get(x).number);
                hash.put("organization",  receipient_selection.get(x).organization);
                receipientList.add(hash);
            }

            receipient_selection_adapter = new SimpleAdapter(getBaseContext(),
                    receipientList,
                    R.layout.recepient_list,
                    new String[]{"name", "organization"},
                    new int[]{R.id.txt_person_name, R.id.txt_organization}
            ) {
                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    final View v = super.getView(position, convertView, parent);

                    // initialize elements
                    ImageView image_container = (ImageView) v.findViewById(R.id.person_image);
                    final ImageButton btn_select = (ImageButton) v.findViewById(R.id.btn_select);

                    Contact contact = new Contact();
                    contact.id = receipientList.get(position).get("id");
                    contact.name = receipientList.get(position).get("name");
                    contact.number = receipientList.get(position).get("number");
                    contact.image = null;
                    btn_select.setTag(contact);

                    Util.animateOnclickView(btn_select, 2, 200, new Util.UtilCallback() {
                        @Override
                        public void method(View view) {
                            // animate fadeout selected contact
                            Util.fadeOut(200, view, new Util.UtilCallback() {
                                @Override
                                public void method(View view) {
                                    Contact contact = (Contact)view.getTag();
                                    receipient_list.add(contact);
                                    displayReceipient();

                                    // remove from array list receipient_selection
                                    Util.removeSelection(receipient_selection, contact.id);

                                    // remove from list view
                                    receipientList.remove(position);
                                    notifyDataSetChanged();
                                }
                            });
                        }
                    });

                    // display image
                    Drawable drawable = getResources().getDrawable(R.drawable.no_image);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    image_container.setImageBitmap(RoundedImageView.getCroppedBitmap(bitmap, 100));

                    return v;
                }
            };
            lv.setAdapter(receipient_selection_adapter);

            // custom dialog
            dialog_contacts = new Dialog(ComposeMessage.this);

            dialog_contacts.requestWindowFeature(Window.FEATURE_NO_TITLE); // remove header
            dialog_contacts.setContentView(customView);
            dialog_contacts.show();

            // close button event listener
            Util.animateOnclickView(btn_close, 1, 200, new Util.UtilCallback() {
                @Override
                public void method(View view) {
                    dialog_contacts.dismiss();
                }
            });
        }
    };

    private void initializeListview() {
        SentMessageList = new ArrayList<HashMap<String, String>>();
        adapter = new SimpleAdapter(getApplicationContext(),
                SentMessageList,
                R.layout.sent_message_list,
                new String[]{"message", "receipient", "datetime_sent"},
                new int[]{R.id.txt_my_message, R.id.txt_receipient, R.id.txt_sent_datetime}
        ){
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ImageView error = (ImageView)v.findViewById(R.id.error);

                // show error icon if failed message failed to send
                if (Objects.equals(SentMessageList.get(position).get("success"), "false")) {
                    error.setVisibility(View.VISIBLE);
                }
                return v;
            }
        };
        sent_message_list_view.setAdapter(adapter);

        // remove line between list view rows
        sent_message_list_view.setDivider(null);
        sent_message_list_view.setDividerHeight(0);
    }

    protected Util.UtilCallback btnSendMessage
            = new Util.UtilCallback() {

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public void method(View view) {
            String warning = "";
            if (Objects.equals(txt_message.getText().toString().trim(), "")) {
                warning = "Text message is required!";
            } else
            if (receipient_list.size() == 0) {
                warning = "No receipient found. please select/add a receipient first...";
            }

            // alert warning message if text message is empty or no receipient added
            if (!warning.equals("")) {
                Util.dialogMessage(
                        ComposeMessage.this,
                        "Warning!",
                        warning,
                        R.mipmap.ic_warning,
                        "Okay",
                        null
                );
                // end method
                return;
            }

            // send message confirmation
            Util.dialogMessage(
                    ComposeMessage.this,
                    "Confirmation",
                    "Are you sure you want to send this message?",
                    R.mipmap.ic_confirmation,
                    "Yes",
                    btnSendMessagePositiveListener,
                    "No",
                    null
            );
        }
    };

    private DialogInterface.OnClickListener btnSendMessagePositiveListener
            = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            // getting text message value before clearing it
            final String message = txt_message.getText().toString();

            // clearing text message input
            txt_message.setText("");

            // disable send button
            btn_send.setClickable(false);

            // hide keyboard
            Util.HideKeyboard(ComposeMessage.this, getBaseContext());

            for (int x = 0; x < receipient_list.size(); x++) {
                final Contact receipient = receipient_list.get(x);

                // Unique broadcast receiver name
                String SENT = "SENT_MESSAGE_"+receipient.id;
                final Intent sentIntent = new Intent(SENT);
                PendingIntent sentPI = PendingIntent.getBroadcast(
                        getApplicationContext(), 0, sentIntent, 0);

                // Register broadcast receiver per message
                final int final_x = x;
                registerReceiver(new BroadcastReceiver(){
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onReceive(Context arg0, Intent arg1) {
                        HashMap sent_message = new HashMap();
                        sent_message.put("message", message);
                        sent_message.put("receipient", "To: "+receipient.name);
                        sent_message.put("datetime_sent", "1 hour ago");
                        switch (getResultCode())
                        {
                            case ComposeMessage.RESULT_OK:
                                // when sms successfully sent
                                sent_message.put("success", "true");
                                break;
                            default:
                                // when sms failed to send
                                sent_message.put("success", "false");
                                break;
                        }
                        SentMessageList.add(sent_message);
                        adapter.notifyDataSetChanged();

                        if (final_x == receipient_list.size()-1) {
                            // enable button back
                            btn_send.setClickable(true);
                        }
                    }
                }, new IntentFilter(SENT));

                SmsManager sms = SmsManager.getDefault();
                sentIntent.putExtra("contactID", receipient.id);
                sms.sendTextMessage(
                        receipient.number,
                        null,
                        txt_message.getText().toString(),
                        sentPI,
                        null
                );
            }
        }
    };


    private void displayReceipient() {

        String htmlScript = "<script>";
        htmlScript += "function removeReceipient(id){";
        // remove element from webview list
        htmlScript += "document.getElementById('receipient-\'+id+\'').remove();";
        // call method removeReceipient from android
        htmlScript += "Android.removeReceipient(id); }";
        htmlScript += "</script>";

        String htmlStyle = "<style>";
        htmlStyle += ".receipient{ margin-left: 25px;font-size: 14px; }";
        htmlStyle += ".remove{ font-size: 4px;margin-left: 5px; }";
        htmlStyle += "font-size: 4px;margin-left: 5px; {";
        htmlStyle += "</style>";

        String htmlText = "<div>";
        htmlText += "<b><span style='text-size:16px;'>To: </span></b>";
        for(int x=0;x<receipient_list.size();x++){
            String id = String.valueOf(receipient_list.get(x).id),
                   name = receipient_list.get(x).name,
                   number = receipient_list.get(x).number;

            htmlText += "<div id='receipient-"+id+"' class='receipient'>"+name+"(+"+number+")";
            htmlText += "<button class='remove' onClick='removeReceipient(\""+id+"\")'>✖</button>";
            htmlText += "</div>";
        }
        htmlText += "</div>";
        String html = "<html>";
        html += "<head>";
        html += htmlScript;
        html += htmlStyle;
        html += "</head>";
        html += "<body>";
        html += "<div>" + htmlText + "</div>";
        html += "</body>";
        html += "</html>";
        txt_to.addJavascriptInterface(new WebAppInterface(this), "Android");
        WebSettings webSettings = txt_to.getSettings();
        webSettings.setJavaScriptEnabled(true);
        txt_to.loadDataWithBaseURL("", html, "text/html", "utf-8", "");

        if (txt_to.getHeight() > 280) {
            ViewGroup.LayoutParams layout = txt_to.getLayoutParams();
            layout.height = 280;
            txt_to.setLayoutParams(layout);
        }
    }

    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void removeReceipient(String id) {
            Contact contact = Util.getContact(receipient_list, id);
            Util.removeSelection(receipient_list, id);
            if (contact == null) return;
            receipient_selection.add(contact);

            HashMap hash = new HashMap();
            hash.put("id",  contact.id);
            hash.put("name",  contact.name);
            hash.put("number",  contact.number);
            hash.put("organization",  contact.organization);
            receipientList.add(hash);
        }
    }

    public void popUpMessage()
    {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        chatHead = new ImageView(this);
        chatHead.setImageResource(R.mipmap.ic_profile);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.width = 250;
        params.height = 250;
        params.x = 0;
        params.y = 300;

        windowManager.addView(chatHead, params);
    }
}
