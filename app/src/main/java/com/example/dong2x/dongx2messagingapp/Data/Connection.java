package com.example.dong2x.dongx2messagingapp.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.dong2x.dongx2messagingapp.MyObjects.Contact;

public class Connection extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "dbContacts.db";
    public static final String TABLE_NAME = "dbContacts.db";

    public Connection(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("" +
                "create table contacts( " +
                    "id integer primary key autoincrement," +
                    "name varchar(100)," +
                    "number varchar(30)," +
                    "organization varchar(100)," +
                    "gender int(1)," +
                    "relationship varchar(100),"+
                    "about varchar(300) " +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists contacts");
    }

    public long addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // fill values
        values.put("name", contact.name);
        values.put("number", contact.number);
        values.put("organization", contact.organization);
        values.put("gender", contact.gender);
        values.put("relationship", contact.relationship);
        values.put("about", contact.about);
        return db.insert("contacts", null, values);
    }

    public Cursor searchContact(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("Select * from contacts WHERE name LIKE '%" + name + "%'", null);
    }

    public long deleteContact(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contacts", "id = ?", new String[] {id});
    }

    public long updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // fill values
        values.put("name", contact.name);
        values.put("number", contact.number);
        values.put("organization", contact.organization);
        values.put("gender", contact.gender);
        values.put("relationship", contact.relationship);
        values.put("about", contact.about);
        return db.update("contacts", values, "id = ?", new String [] { String.valueOf(contact.id) });
    }

}
