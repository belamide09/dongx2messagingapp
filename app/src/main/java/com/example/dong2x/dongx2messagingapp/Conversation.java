package com.example.dong2x.dongx2messagingapp;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.dong2x.dongx2messagingapp.MyObjects.Contact;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;

public class Conversation extends AppCompatActivity {
    ImageView person_image;
    ImageButton send_button;
    ListView conversation_list_view;
    EditText txt_message;
    Gson gson;
    Contact contact;
    ArrayList<HashMap<String, String>> MessageList;
    SimpleAdapter adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_conversation);
            actionBar.setTitle("Conversations");
        }

        gson = new Gson();

        TextView txt_person_name = (TextView)findViewById(R.id.txt_person_name);
        TextView txt_contact_number = (TextView)findViewById(R.id.txt_contact_no);

        Intent intent = getIntent();

        contact = gson.fromJson(intent.getStringExtra("contact"), Contact.class);

        // display person detail
        txt_person_name.setText(contact.number);
        txt_contact_number.setText(contact.name);

        conversation_list_view = (ListView)findViewById(R.id.conversation_listview);
        person_image = (ImageView)findViewById(R.id.person_image);
        send_button = (ImageButton)findViewById(R.id.btn_send_message);
        txt_message = (EditText)findViewById(R.id.txt_message);

        getConversations();

        // display image
        Drawable drawable = getResources().getDrawable(R.drawable.no_image);
        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        person_image.setImageBitmap(RoundedImageView.getCroppedBitmap(bitmap, 500));

        // animate button when click
        Util.animateOnclickView(send_button, 1, 200, btnSendClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Util.UtilCallback btnSendClickListener
            = new Util.UtilCallback() {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public void method(View view) {
            // alert warning message if text message is empty
            if (Objects.equals(txt_message.getText().toString().trim(), "")) {
                Util.dialogMessage(
                        Conversation.this,
                        "Warning!",
                        "Text message is required!",
                        R.mipmap.ic_warning,
                        "Okay",
                        null
                );
                // end method
                return;
            }

            // send message confirmation
            Util.dialogMessage(
                    Conversation.this,
                    "Confirmation",
                    "Are you sure you want to send this message?",
                    R.mipmap.ic_confirmation,
                    "Yes",
                    btnSendMessagePositiveListener,
                    "No",
                    null
            );
        }
    };

    private DialogInterface.OnClickListener btnSendMessagePositiveListener
            = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            final String message = txt_message.getText().toString();

            // clearing text message input
            txt_message.setText("");

            // disable send button
            send_button.setClickable(false);

            // hide keyboard
            Util.HideKeyboard(Conversation.this, getBaseContext());

            // disable send button
            send_button.setClickable(false);
            send_button.setBackgroundResource(R.drawable.button_bg_disable);

            // Unique broadcast receiver name
            String SENT = "SENT_MESSAGE";
            Intent sentIntent = new Intent(SENT);
            PendingIntent sentPI = PendingIntent.getBroadcast(
                    getApplicationContext(), 0, sentIntent, 0);

            // Register broadcast receiver
            registerReceiver(new BroadcastReceiver(){
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    HashMap sent_message = new HashMap();
                    sent_message.put("message", message);
                    sent_message.put("sender", constant.id);
                    switch (getResultCode())
                    {
                        case ComposeMessage.RESULT_OK:
                            // when sms successfully sent
                            sent_message.put("success", "true");
                            break;
                        default:
                            // when sms failed to send
                            sent_message.put("success", "false");
                            break;
                    }
                    MessageList.add(sent_message);
                    adapter.notifyDataSetChanged();

                    // enable send button again
                    send_button.setClickable(true);

                    // scroll to bottom
                    int scroll_position = (conversation_list_view.getCount()-1);
                    conversation_list_view.smoothScrollToPosition(scroll_position);
                }
            }, new IntentFilter(SENT));

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(
                    contact.number,
                    null,
                    txt_message.getText().toString(),
                    sentPI,
                    null
            );
        };
    };

    private void getConversations() {
        MessageList = new ArrayList<HashMap<String, String>>();

        HashMap hash = new HashMap();
        hash.put("message", "As usual, we use ViewHolder pattern for efficiency when recreating each items in the Listview. LayoutParams are for designing the Layout left or right aligned according to the Chat Message Send or Received. A dummy boolean value is used as the property to check whether its Send or Received for simplicity .");
        hash.put("sender", constant.id);
        MessageList.add(hash);
        HashMap hash2 = new HashMap();
        hash2.put("message", "Hi this is my message 2 hehehehe ");
        hash2.put("sender", contact.id);
        MessageList.add(hash2);
        HashMap hash3 = new HashMap();
        hash3.put("message", "Another message here ehehhehehe");
        hash3.put("sender", contact.id);
        MessageList.add(hash3);
        HashMap hash4 = new HashMap();
        hash4.put("message", "Ops sorry late reply");
        hash4.put("sender", constant.id);
        MessageList.add(hash4);

        Random ran = new Random();
        String ids [] = {contact.id , "99"};

        for (int x = 0; x < 10; x++) {
            HashMap hash5 = new HashMap();
            hash5.put("message", "Message: " + String.valueOf(x+1));
            hash5.put("sender", ids[ran.nextInt(2)]);
            MessageList.add(hash5);
        }

        adapter = new SimpleAdapter(getApplicationContext(),
                MessageList,
                R.layout.conversation_list,
                new String[]{"message", "message"},
                new int[]{R.id.txt_my_message, R.id.txt_other_message}
        ){
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                RelativeLayout my_message = (RelativeLayout)v.findViewById(R.id.my_message_container);
                RelativeLayout other_message = (RelativeLayout)v.findViewById(R.id.other_message_container);
                ImageView error = (ImageView)v.findViewById(R.id.error), image;

                HashMap message = MessageList.get(position);

                if (message.get("sender").equals(contact.id)) {
                    other_message.setVisibility(View.VISIBLE);
                    my_message.setVisibility(View.GONE);
                    image = (ImageView)v.findViewById(R.id.other_image);
                } else {
                    other_message.setVisibility(View.GONE);
                    my_message.setVisibility(View.VISIBLE);
                    image = (ImageView)v.findViewById(R.id.my_image);
                }

                if (MessageList.get(position).get("success") != null &&
                        Objects.equals(MessageList.get(position).get("success"), "false")) {
                    error.setVisibility(View.VISIBLE);
                }

                // display image
                Drawable drawable = getResources().getDrawable(R.drawable.no_image);
                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
                image.setImageBitmap(RoundedImageView.getCroppedBitmap(bitmap, 500));
                return v;
            }
        };
        // append data in list view

        conversation_list_view.setAdapter(adapter);
        // remove line between list view rows
        conversation_list_view.setDivider(null);
        conversation_list_view.setDividerHeight(0);
    }
}
