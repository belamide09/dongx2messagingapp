package com.example.dong2x.dongx2messagingapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.dong2x.dongx2messagingapp.Data.Connection;
import com.example.dong2x.dongx2messagingapp.MyObjects.Contact;
import com.google.gson.Gson;

public class PersonDetail extends AppCompatActivity {
    ImageView person_image;
    ImageButton btn_conversation, btn_call, btn_action, btn_cancel;
    TextView txt_person_name, txt_gender, edit_gender, txt_contact_no, txt_organization, txt_relationship, txt_about;
    EditText edit_person_name, edit_contact_no, edit_organization, edit_relationship, edit_about;

    String action;

    Gson gson;
    Contact contact;

    Connection connection;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_profile);
            actionBar.setTitle("Contact Detail");
        }

        gson = new Gson();
        connection = new Connection(getBaseContext());

        Intent intent = getIntent();
        action = intent.getStringExtra("action");

        contact = gson.fromJson(intent.getStringExtra("contact"), Contact.class);

        btn_conversation = (ImageButton) findViewById(R.id.btn_conversations);
        btn_call = (ImageButton) findViewById(R.id.btn_select);
        btn_action = (ImageButton) findViewById(R.id.btn_action);
        btn_cancel = (ImageButton) findViewById(R.id.btn_cancel);

        // all the text view element
        txt_person_name = (TextView) findViewById(R.id.txt_person_name);
        txt_contact_no = (TextView) findViewById(R.id.txt_contact_no);
        txt_gender = (TextView) findViewById(R.id.txt_gender);
        txt_organization = (TextView) findViewById(R.id.txt_organization);
        txt_relationship = (TextView) findViewById(R.id.txt_relationship);
        txt_about = (TextView) findViewById(R.id.txt_about);

        edit_person_name = (EditText) findViewById(R.id.edit_person_name);
        edit_contact_no = (EditText) findViewById(R.id.edit_person_contact_no);
        edit_gender = (TextView) findViewById(R.id.edit_gender);
        edit_organization = (EditText) findViewById(R.id.edit_organization);
        edit_relationship = (EditText) findViewById(R.id.edit_relationship);
        edit_about = (EditText) findViewById(R.id.edit_about);

        person_image = (ImageView) findViewById(R.id.person_image);

        btn_conversation = (ImageButton) findViewById(R.id.btn_conversations);
        btn_call = (ImageButton) findViewById(R.id.btn_select);

        if (action.equals("view")) {
            // display contact detail
            txt_person_name.setText(contact.name);
            txt_contact_no.setText(contact.number);
            txt_organization.setText(contact.organization);
            txt_gender.setText(constant.gender[Integer.parseInt(contact.gender)]);
        }

        // add event listeners to image button(s)
        Util.animateOnclickView(btn_conversation, 2, 100, new Util.UtilCallback() {
            @Override
            public void method(View view) {
                Intent intent = new Intent(getBaseContext(), Conversation.class);
                intent.putExtra("contact", gson.toJson(contact));
                startActivity(intent);
            }
        });
        Util.animateOnclickView(btn_action, 2, 100, btnActionEventListener);
        Util.animateOnclickView(btn_cancel, 2, 100, btnCancelEventListener);
        Util.animateOnclickView(btn_call, 2, 100, null);

        // initialize image drawable and display to image view
        Drawable drawable = getResources().getDrawable(R.drawable.no_image);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        person_image.setImageBitmap(RoundedImageView.getCroppedBitmap(bitmap, 500));

        // convert text view to radio select buttons
        Util.useRadioButton(
                PersonDetail.this,
                LinearLayout.HORIZONTAL,
                R.array.gender_array,
                edit_gender
        );

        switch (action) {
            case "add":
                onAddProccess();
                break;
            case "view":
                onViewProccess();
                break;
            case "update":
                onEditProccess();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_back:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


    private void onAddProccess() {
        btn_conversation.setVisibility(View.INVISIBLE);
        btn_call.setVisibility(View.INVISIBLE);
        btn_action.setImageResource(R.mipmap.ic_save);
    }

    private void onViewProccess() {
        /* assign information */
        FillData();

        btn_conversation.setVisibility(View.VISIBLE);
        btn_call.setVisibility(View.VISIBLE);

        // hide all person detail text view input
        txt_person_name.setVisibility(View.VISIBLE);
        txt_contact_no.setVisibility(View.VISIBLE);
        txt_organization.setVisibility(View.VISIBLE);
        txt_relationship.setVisibility(View.VISIBLE);
        txt_about.setVisibility(View.VISIBLE);
        txt_gender.setVisibility(View.VISIBLE);

        // show all person detail  edit text input
        edit_person_name.setVisibility(View.GONE);
        edit_contact_no.setVisibility(View.GONE);
        edit_organization.setVisibility(View.GONE);
        edit_relationship.setVisibility(View.GONE);
        edit_about.setVisibility(View.GONE);
        edit_gender.setVisibility(View.GONE);

        btn_action.setImageResource(R.mipmap.ic_edit);
        btn_cancel.setVisibility(View.GONE);

        btn_conversation.setVisibility(View.VISIBLE);
        btn_call.setVisibility(View.VISIBLE);

        ScrollTop();
    }

    private void onEditProccess() {
        /* assign information */
        FillData();

        // hide all person detail text view input
        edit_person_name.setVisibility(View.VISIBLE);
        edit_contact_no.setVisibility(View.VISIBLE);
        edit_organization.setVisibility(View.VISIBLE);
        edit_relationship.setVisibility(View.VISIBLE);
        edit_about.setVisibility(View.VISIBLE);
        edit_gender.setVisibility(View.VISIBLE);

        // show all person detail  edit text input
        txt_person_name.setVisibility(View.GONE);
        txt_contact_no.setVisibility(View.GONE);
        txt_organization.setVisibility(View.GONE);
        txt_relationship.setVisibility(View.GONE);
        txt_about.setVisibility(View.GONE);
        txt_gender.setVisibility(View.GONE);

        btn_action.setImageResource(R.mipmap.ic_save);
        btn_cancel.setVisibility(View.VISIBLE);

        btn_conversation.setVisibility(View.INVISIBLE);
        btn_call.setVisibility(View.INVISIBLE);
    }

    private void FillData() {
        edit_person_name.setText(contact.name);
        edit_contact_no.setText(contact.number);
        edit_organization.setText(contact.organization);
        edit_relationship.setText(contact.relationship);
        edit_about.setText(contact.about);
        edit_gender.setText(contact.gender.equals("1") ? "Male" : "Female");

        txt_person_name.setText(contact.name);
        txt_contact_no.setText(contact.number);
        txt_organization.setText(contact.organization);
        txt_relationship.setText(contact.relationship);
        txt_about.setText(contact.about);
        txt_gender.setText(contact.gender.equals("1") ? "Male" : "Female");

        ScrollTop();
    }

    private Util.UtilCallback btnCancelEventListener
            = new Util.UtilCallback() {
        @Override
        public void method(View view) {
            onViewProccess();
            action = "view";
        }
    };

    private Util.UtilCallback btnActionEventListener
            = new Util.UtilCallback() {
        @Override
        public void method(View view) {
            switch (action) {
                case "view":
                    btn_action.setImageResource(R.mipmap.ic_save);
                    action = "update";
                    onEditProccess();
                    break;
                case "add":
                    addContact();
                    break;
                case "edit":
                    btn_action.setImageResource(R.mipmap.ic_save);
                    action = "update";
                    break;
                case "update":
                    updateContact();
                    break;
            }
        }
    };

    private boolean hasEmpty() {
        return edit_person_name.getText().toString().trim().equals("") ||
                edit_contact_no.getText().toString().trim().equals("") ||
                edit_gender.getText().toString().trim().equals("") ||
                edit_organization.getText().toString().trim().equals("") ||
                edit_relationship.getText().toString().trim().equals("") ||
                edit_about.getText().toString().trim().equals("");
    }

    private void addContact() {
        /* don't allowed empty */
        if (hasEmpty()) {
            Util.dialogMessage(
                    PersonDetail.this,
                    "Warning!",
                    "All field(s) are required!",
                    R.mipmap.ic_warning,
                    "Okay",
                    null
            );
            return;
        }

        Util.dialogMessage(
                PersonDetail.this,
                "Confirmation",
                "Are you sure you want to add this contact?",
                R.mipmap.ic_confirmation,
                "Yes",
                confirmSaveContact,
                "No",
                null
        );
    }

    private void updateContact() {
        /* don't allowed empty */
        if (hasEmpty()) {
            Util.dialogMessage(
                    PersonDetail.this,
                    "Warning!",
                    "All field(s) are required!",
                    R.mipmap.ic_warning,
                    "Okay",
                    null
            );
            return;
        }

        Util.dialogMessage(
                PersonDetail.this,
                "Confirmation",
                "Are you sure you want to update this contact?",
                R.mipmap.ic_confirmation,
                "Yes",
                confirmSaveContact,
                "No",
                null
        );
    }

    private Dialog.OnClickListener confirmSaveContact
            = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            Contact contactDetail = new Contact();
            contactDetail.name = edit_person_name.getText().toString();
            contactDetail.number = edit_contact_no.getText().toString();
            contactDetail.organization = edit_organization.getText().toString();
            contactDetail.gender = (edit_gender.getText().toString().equals("Male")) ? "1" : "2";
            contactDetail.relationship = edit_relationship.getText().toString();
            contactDetail.about = edit_about.getText().toString();

            long inserted = -1;

            /* check if process is add or update */
            if (action.equals("add")) {
                inserted = connection.addContact(contactDetail);
            } else {
                contactDetail.id = contact.id;
                inserted = connection.updateContact(contactDetail);
            }

            /* if failed to insert contact */
            if (inserted == -1) {
                Util.dialogMessage(
                        PersonDetail.this,
                        "Error!",
                        "Failed to save contact. Please try again...",
                        R.mipmap.ic_error,
                        "Okay",
                        null
                );
                return;
            }

            contactDetail.id = String.valueOf(inserted);
            contact = contactDetail;
            action = "view";

            onViewProccess();
        }
    };

    private void ScrollTop(){
        ScrollView scroll = (ScrollView) findViewById(R.id.person_detail_scrollview);
        scroll.setFocusableInTouchMode(true);
        scroll.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
    }

}
