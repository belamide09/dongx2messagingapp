package com.example.dong2x.dongx2messagingapp.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.dong2x.dongx2messagingapp.Conversation;
import com.example.dong2x.dongx2messagingapp.Data.Connection;
import com.example.dong2x.dongx2messagingapp.PersonDetail;
import com.example.dong2x.dongx2messagingapp.R;
import com.example.dong2x.dongx2messagingapp.RoundedImageView;
import com.example.dong2x.dongx2messagingapp.Util;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;


public class Contacts extends Fragment {
    Dialog dialog_contacts;
    View viewContactOption;
    Connection connection;
    SimpleAdapter adapter;
    EditText txt_search;
    ListView contacts;
    Gson gson;

    ArrayList<HashMap<String, String>> ContactList;
    int prev;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_contacts, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connection = new Connection(getContext());
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        gson = new Gson();
        contacts = (ListView) getView().findViewById(R.id.contact_list);
        txt_search = (EditText) getView().findViewById(R.id.txt_search_contact_person);
        ContactList();

        dialog_contacts = null;
        prev = -1;
    }

    private void ContactList() {

        ContactList = new ArrayList<HashMap<String, String>>();

        Cursor res = connection.searchContact(txt_search.getText().toString());

        /* stop method if no result found */
        if (res.getCount() == 0){return;}

        while (res.moveToNext()) {
            HashMap<String, String> student = new HashMap<String, String>();
            student.put("id", res.getString(res.getColumnIndex("id")));
            student.put("name", res.getString(res.getColumnIndex("name")));
            student.put("number", res.getString(res.getColumnIndex("number")));
            student.put("organization", res.getString(res.getColumnIndex("organization")));
            student.put("gender", res.getString(res.getColumnIndex("gender")));
            student.put("relationship", res.getString(res.getColumnIndex("relationship")));
            student.put("about", res.getString(res.getColumnIndex("about")));
            ContactList.add(student);
        }

        adapter = new SimpleAdapter(getContext(),
                ContactList,
                R.layout.contact_list,
                new String[]{"name", "organization"},
                new int[]{R.id.txt_person_name, R.id.txt_organization}
        ) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                // initialize elements
                ImageView image_container = (ImageView) v.findViewById(R.id.person_image);
                ImageButton btn_conversation = (ImageButton) v.findViewById(R.id.btn_send_message);
                ImageButton btn_call = (ImageButton) v.findViewById(R.id.btn_select);

                // pass data as tag
                v.setTag(ContactList.get(position));
                btn_conversation.setTag(ContactList.get(position));

                // apply element(s) event listeners
                v.setOnLongClickListener(contactOnLongClickListener);
                v.setOnTouchListener(contactOnTouchListener);

                Util.animateOnclickView(v, 1, 100, contactClickListener);
                Util.animateOnclickView(btn_conversation, 2, 100, btnConversationClickListener);
                Util.animateOnclickView(btn_call, 2, 100, null);
                if (v.isSelected()) {
                    Log.d("Messages", "__SELECTED__");
                }

                // display image
                Drawable drawable = getResources().getDrawable(R.drawable.no_image);
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                image_container.setImageBitmap(RoundedImageView.getCroppedBitmap(bitmap, 100));

                return v;
            }
        };
        contacts.setAdapter(adapter);
    }

    private Util.UtilCallback contactClickListener
            = new Util.UtilCallback() {
        @Override
        public void method(View view) {
            HashMap hashmap =(HashMap)view.getTag();
            Intent intent = new Intent(getContext(), PersonDetail.class);
            intent.putExtra("contact", gson.toJson(hashmap));
            intent.putExtra("action", "view");
            startActivity(intent);
        }
    };

    private Util.UtilCallback btnConversationClickListener
            = new Util.UtilCallback() {
        @Override
        public void method(View view) {
            HashMap hashmap =(HashMap)view.getTag();
            Intent intent = new Intent(getContext(), Conversation.class);
            intent.putExtra("contact", gson.toJson(hashmap));
            startActivity(intent);
        }
    };

    private  View.OnTouchListener contactOnTouchListener
            = new View.OnTouchListener() {
        @Override
        public boolean onTouch(final View view, MotionEvent motionEvent) {
            view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
            Util.Delay(50, new Util.UtilCallback() {
                @Override
                public void method(View v) {
                    view.getBackground().clearColorFilter();
                }
            });
            return false;
        }
    };

    private View.OnLongClickListener contactOnLongClickListener
            = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(final View view) {
            HashMap hashmap =(HashMap)view.getTag();

            /* if dialog_contacts is already initialized */
            if (dialog_contacts != null) {
                TextView textview_name = (TextView) dialog_contacts.findViewById(R.id.lbl_name);
                textview_name.setText(hashmap.get("name").toString());
                dialog_contacts.show();
            } else {
                // Initialize a new instance of LayoutInflater service
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                viewContactOption = inflater.inflate(R.layout.popup_contact_option, null);

                TextView textview_name = (TextView) viewContactOption.findViewById(R.id.lbl_name);
                textview_name.setText(hashmap.get("name").toString());

                dialog_contacts = new Dialog(getContext());
                dialog_contacts.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog_contacts.requestWindowFeature(Window.FEATURE_NO_TITLE); // remove header
                dialog_contacts.setContentView(viewContactOption);
                dialog_contacts.show();
            }

            RelativeLayout relativelayout_view = (RelativeLayout)viewContactOption.findViewById(R.id.relativelayout_view);
            RelativeLayout relativelayout_edit = (RelativeLayout)viewContactOption.findViewById(R.id.relativelayout_edit);
            RelativeLayout relativelayout_remove = (RelativeLayout)viewContactOption.findViewById(R.id.relativelayout_remove);

            /* if click view */
            Util.animateOnclickView(relativelayout_view, 1, 100, new Util.UtilCallback() {
                @Override
                public void method(View v) {
                    HashMap hashmap =(HashMap)view.getTag();
                    Intent intent = new Intent(getContext(), PersonDetail.class);
                    intent.putExtra("contact", gson.toJson(hashmap));
                    intent.putExtra("action", "view");
                    startActivity(intent);

                    /* close dialog */
                    dialog_contacts.dismiss();
                }
            });

            /* if click edit */
            Util.animateOnclickView(relativelayout_edit, 1, 100, new Util.UtilCallback() {
                @Override
                public void method(View v) {
                    HashMap hashmap =(HashMap)view.getTag();
                    Intent intent = new Intent(getContext(), PersonDetail.class);
                    intent.putExtra("contact", gson.toJson(hashmap));
                    intent.putExtra("action", "update");
                    startActivity(intent);

                    /* close dialog */
                    dialog_contacts.dismiss();
                }
            });

            /* if click remove */
            Util.animateOnclickView(relativelayout_remove, 1, 100, new Util.UtilCallback() {
                @Override
                public void method(View v) {
                    /* close dialog */
                    dialog_contacts.dismiss();

                    Util.dialogMessage(
                            getContext(),
                            "Confirmation",
                            "Are you sure you want to remove this contact?",
                            R.mipmap.ic_confirmation,
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    HashMap hashmap =(HashMap)view.getTag();
                                    RemoveContact(view, hashmap);
                                }
                            },
                            "No",
                            null
                    );
                }
            });

            return false;
        }
    };

    private void RemoveContact(View view, final HashMap hashmap) {
        /* delete contact from the database */
        connection.deleteContact(hashmap.get("id").toString());

        /* fadeout deleted contact */
        Util.fadeOut(200, view, new Util.UtilCallback() {
            @Override
            public void method(View view) {
                ContactList.remove(ContactList.indexOf(hashmap));
                adapter.notifyDataSetChanged();
            }
        });
    }
}
