package com.example.dong2x.dongx2messagingapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.example.dong2x.dongx2messagingapp.fragment.*;

public class Home extends AppCompatActivity {
    Fragment current;
    ImageButton btn_contacts, btn_new, btn_compose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_contacts);
            actionBar.setTitle("Contact List");
        }

        btn_contacts = (ImageButton) findViewById(R.id.btn_contacts);
        btn_new = (ImageButton) findViewById(R.id.btn_new);
        btn_compose = (ImageButton) findViewById(R.id.btn_compose);
        current = new Contacts();

        Util.animateOnclickView(btn_contacts, 2, 200, btnOpenFragment);
        Util.animateOnclickView(btn_new, 2, 200, new Util.UtilCallback() {
            @Override
            public void method(View view) {
                Intent intent = new Intent(getBaseContext(), PersonDetail.class);
                intent.putExtra("action", "add");
                startActivity(intent);
            }
        });
        Util.animateOnclickView(btn_compose, 2, 200, new Util.UtilCallback() {
            @Override
            public void method(View view) {
                Intent intent = new Intent(getBaseContext(), ComposeMessage.class);
                startActivity(intent);
            }
        });
    }

    private Util.UtilCallback btnOpenFragment
            = new Util.UtilCallback() {
        @Override
        public void method(View view) {
            Fragment fragment = null;
            switch (view.getId()) {
                case R.id.btn_contacts:
                    fragment = new Contacts();
                    break;
            }
            if (current.getId() == fragment.getId()) return;
            current = new Contacts();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.menu_fragment, fragment);
            ft.commit();
        }
    };
}
