package com.example.dong2x.dongx2messagingapp.MyObjects;

import android.graphics.Bitmap;

public class Contact {

    public String id, name, number, organization, gender, relationship, about;
    public Bitmap image;
}
