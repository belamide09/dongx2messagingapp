package com.example.dong2x.dongx2messagingapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.dong2x.dongx2messagingapp.MyObjects.Contact;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Util extends Activity {

    public interface UtilCallback {
        public void method(View view);
    }

    public static void Delay(int delay, final UtilCallback callback) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                View view = null;
                callback.method(view);
            }
        }, delay);
    }

    /**
     * @param duration => how long will the animation ends
     * @param v => the view
     * @param callback => do something after animation ends
     */
    public static void fadeOut(int duration, final View v, final UtilCallback callback) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(duration);
        fadeOut.setDuration(duration);
        v.startAnimation(fadeOut);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                callback.method(v);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    /**
     * @param ctx -> the context activity
     * @param id -> the array inside values/strings.xml
     * @param textRadio -> the user element that display selected radio button value
     */
    public static void useRadioButton(
            final Context ctx,
            final int orientation,
            final int id,
            final TextView textRadio
    ) {
        textRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                final Dialog dialog = new Dialog(ctx);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radio_buttons);

                RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radio_group);
                rg.setOrientation(orientation);

                List<String> stringList = Arrays.asList(ctx.getResources().getStringArray(id));
                for(int i = 0 ; i<stringList.size() ; i++) {
                    final RadioButton rb = new RadioButton(ctx);
                    rb.setText(stringList.get(i));
                    rb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            textRadio.setText(rb.getText().toString());
                            textRadio.setTextColor(Color.BLACK);
                            dialog.dismiss();
                        }
                    });
                    rg.addView(rb);

                    // restore selected radio button if open again
                    if (textRadio.getText().toString().equals(stringList.get(i))) {
                        rb.setSelected(true);
                        rb.performClick();
                    }
                }
                if (dialog.getWindow() != null) {
                    int bg = R.drawable.radio_buttons_bg;
                    dialog.getWindow().setBackgroundDrawableResource(bg);
                }
                dialog.show();
            }
        });
    }

    /**
     * @param ctx => the context activity
     * @param title => the title of alert dialog
     * @param message => the message of alert dialog
     * @param icon => the icon of alert dialog
     * @param positive_button => the display of positive button
     * @param positive_button_click_event => the click event listener of positive button
     * @param negative_button => the dispaly of negative button
     * @param negative_button_click_event => the click event listenr of negative button
     */
    public static void dialogMessage(
            Context ctx,
            String title,
            String message,
            int icon,
            String positive_button,
            DialogInterface.OnClickListener positive_button_click_event,
            String negative_button,
            DialogInterface.OnClickListener negative_button_click_event
    ) {
        AlertDialog.Builder alert_builder = new AlertDialog.Builder(ctx);
        alert_builder.setMessage(message)
                .setTitle(title)
                .setCancelable(true)
                .setIcon(icon)
                .setPositiveButton(positive_button, positive_button_click_event)
                .setNegativeButton(negative_button, negative_button_click_event);
        AlertDialog alert = alert_builder.create();
        alert.show();
    }

    /**
     * @param ctx => the context activity
     * @param title => the title of alert dialog
     * @param message => the message of alert dialog
     * @param icon => the icon of alert dialog
     * @param positive_button => the display of positive button
     * @param positive_button_click_event => the click event listener of positive button
     */
    public static void dialogMessage(
            Context ctx,
            String title,
            String message,
            int icon,
            String positive_button,
            DialogInterface.OnClickListener positive_button_click_event
    ) {
        AlertDialog.Builder alert_builder = new AlertDialog.Builder(ctx);
        alert_builder.setMessage(message)
                .setTitle(title)
                .setIcon(icon)
                .setPositiveButton(positive_button, positive_button_click_event);
        AlertDialog alert = alert_builder.create();
        alert.show();
    }

    /**
     * @param ctx => the context activity
     * @param title => the title of alert dialog
     * @param message => the message of alert dialog
     * @param icon => the icon of alert dialog
     */
    public static void dialogMessage(
            Context ctx,
            String title,
            String message,
            int icon
    ) {
        AlertDialog.Builder alert_builder = new AlertDialog.Builder(ctx);
        alert_builder.setMessage(message)
                .setTitle(title)
                .setIcon(icon);
        AlertDialog alert = alert_builder.create();
        alert.show();
    }

    /**
     *
     * @param view -> the button to be animated
     * @param type -> 1: change color, 2: grow and shrink
     * @param callback -> what to do after view animation finish
     */
    public static void animateOnclickView(final View view, final int type, final int duration, final UtilCallback callback) {

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AnimationSet animation = new AnimationSet(true);
                switch (type) {
                    case 1:
                        ScaleAnimation normal = new ScaleAnimation(1.0f, 1.0f, 1.0f, 1.0f);
                        animation.addAnimation(normal);
                        v.startAnimation(animation);
                        break;
                    case 2:
                        ScaleAnimation growAnimation = new ScaleAnimation(
                                1.0f, 1.10f, 1.0f, 1.10f,
                                Animation.RELATIVE_TO_SELF,
                                0.5f,
                                Animation.RELATIVE_TO_SELF,
                                0.5f
                        );
                        ScaleAnimation shrinkAnimation = new ScaleAnimation(
                                1.10f, 1.0f, 1.10f, 1.0f,
                                Animation.RELATIVE_TO_SELF,
                                0.5f,
                                Animation.RELATIVE_TO_SELF,
                                0.5f
                        );
                        animation.addAnimation(growAnimation);
                        animation.addAnimation(shrinkAnimation);
                        break;
                }
                animation.setDuration(duration);
                v.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        switch (type) {
                            case 1:
                                v.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                                break;
                            case 2:
                                break;
                        }
                    }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        switch (type) {
                            case 1:
                                v.getBackground().clearColorFilter();
                                break;
                            case 2:
                                break;
                        }
                        if (callback == null){ return; }
                        callback.method(view);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                });
            }
        });
    }

    public static Contact getContact(ArrayList<Contact> list,  String id) {
        Contact contact = null;
        for(int x=0;x<list.size();x++){
            if (list.get(x).id.equals(id)) { contact = list.get(x); }
        }
        return contact;
    }
    public static void removeSelection(ArrayList<Contact> list,  String id) {
        for(int x=0;x<list.size();x++){
            if (list.get(x).id.equals(id)) { list.remove(x); }
        }
    }

    public static void HideKeyboard(Activity activity, Context ctx) {
        View view = activity.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
